# Assignment 4

## Mobile Wireframes

Create a wireframe for an online grocery app with following screens:

1.  Login with social login
2.  Sign up with social sign up
3.  Homepage
4.  UI of respective grocery
5.  UI for cart
6.  UI for checkout process

## Desktop Wireframes

Create a wireframe for an admin portal for online grocery app with following screens:

1.  CRUD for adding groceries
2.  CRUD for adding categories of groceries
3.  CRUD for showing registered users
4.  UI for listing of orders placed from the app

You can create these wireframes using [FigJam](https://www.figma.com/figjam/) software. 
