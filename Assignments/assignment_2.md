# Assignment 2
## Website and Mobile Wireframe
Create a wireframe for an service based website and its mobile version (IT Services) with following screens:
1.  Homepage: Sections of homepage will be
    1. Landing section
    2. How we work
    3. Our offerings
    4. Our clientele
    4. Testimonials
    5. About us
    6. Footer
2.  UI of respective service page
3.  Contact us
4.  UI for About us page
5.  UI for portofio/Our work

You can create these wireframes using [Balsamiq](https://balsamiq.com/) software.

