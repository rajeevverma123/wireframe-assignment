# Assignment 1

## Mobile Wirefames

Create a wireframe for an e-commerce app (Electronic Devices) with following screens:

1. Login screen: 2 Variants
    1. Login with email ID and password
    2. Login with phone number and OTP
2. Registration
3. Homepage showing list of devices
4. Respective detailed page of devices

## Desktop Wireframes

Create a wireframe for an e-commerce admin portal (Electronic Devices) with following screens:

1.  CRUD for adding devices
2.  CRUD for adding categories of devices
3.  CRUD for showing registered users


Use real names and images to make it attractive, You can create these wireframes using [Balsamiq](https://balsamiq.com/) software. 
