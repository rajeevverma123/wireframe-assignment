# Assignment 3
## Website & Mobile Wireframe
Create a wireframe for an online travel service app and website with following screens:
1.  Homepage for searching of flights/train/bus
2.  Login with social login
3.  Registration with social sign up
4.  UI for list of flights
5.  UI for check out process
6.  UI for my bookings

You can create these wireframes using [FigJam](https://www.figma.com/figjam/) software.
